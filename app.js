const http = require('http');
var admin = require('firebase-admin');

var serviceAccount = require("./service-account-file.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://ichat-de350.firebaseio.com"
});
http.createServer(function(request, response) {
  response.writeHead(200, {'Content-Type': 'text/plain'});
  response.end("Hello, World! version 2\n");
}).listen(process.env.PORT);

console.log('App is running...');